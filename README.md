### httpd script
```bash

# connecting to ec2
> ssh -i ~/.ssh/ch9_shared.pem ec2-user@34.254.239.71

# command to send script file

 scp -i <secure_key> <file> <user>@<IP_address>:<path_directory>

 > scp -i ~/.ssh/ch9_shared.pem httpd_script.sh ec2-user@34.254.239.71:/home/ec2-user/arslan.sh

# command to send html for website
 scp -i <secure_key> -r <folder> <user>@<IP_address>:<file_directory>

 > scp -i ~/.ssh/ch9_shared.pem -r arslan_website ec2-user@34.254.239.71:/home/ec2-user/

 # command to run bash script
 ssh -i <secure_key> <user>@<IP_address>: bash <bash_file_path>

 > ssh -i ~/.ssh/ch9_shared.pem ec2-user@34.254.239.71 bash /home/ec2-user/arslan.sh
 
 ```
