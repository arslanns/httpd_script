# update the repositories 
sudo yum update

# install ngixn
sudo yum install -y httpd.x86_64

# start apache
sudo systemctl start httpd.service

# enable apache
sudo systemctl enable httpd.service

# moving html file to corect folder
sudo mv ~/arslan_website/index.html /var/www/html/index.html

# restart apache to load new webpage
sudo systemctl restart httpd.service